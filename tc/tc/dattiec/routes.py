from flask import flash,render_template, session, redirect, request, url_for
from wedding import app, db
from .models import Dattiec
from wedding.sanh.models import Sanh
from .forms import BookTiec
from sqlalchemy import or_

import secrets
import os

@app.route('/dattiec', methods=['GET','POST'])
def dattiec():  
    sanh=Sanh.query.all()       
    form=BookTiec(request.form)
    return render_template('dattiec/dattiec.html',form=form, sanh=sanh)

@app.route('/booktiec', methods = ['GET','POST'])
def booktiec():   
    sanh=Sanh.query.all()    
    form=BookTiec()
    if form.validate_on_submit(): 
        tenchure = form.tenchure.data
        tencodau = form.tencodau.data       
        sdt = form.sdt.data    
        ngay = form.ngay.data        
        sanh = request.form.get('sanh')

        
        dt = Dattiec(tenchure=tenchure, tencodau=tencodau, sdt=sdt,  ngay=ngay, Sanh_id=sanh)
        db.session.add(dt)
        db.session.commit()
        
        
        flash(f'Đặt tiệc thành công','success') 
        return redirect(url_for('dattiec'))
    flash(f'Fail','danger') 
    return render_template('dattiec/dattiec.html',form=form, sanh=sanh)

@app.route('/updatetiec/<int:id>', methods=['GET','POST'])
def updatetiec(id):
    form = BookTiec()
    dt = Dattiec.query.get_or_404(id)
    sanh=Sanh.query.all()
    s=request.form.get('sanh')
    b=request.form.get('ban')
    
    if form.validate_on_submit():
        dt.tenchure = form.tenchure.data 
        dt.tencodau = form.tencodau.data
        dt.sdt = form.sdt.data
        dt.soluongban = b
        dt.ngay = form.ngay.data
        dt.Sanh_id=s
        flash('Tiệc cưới đã được cập nhật','success')
        db.session.commit()
        return redirect(url_for('dstieccuoi'))
    form.tenchure.data = dt.tenchure
    form.tencodau.data = dt.tencodau
    form.sdt.data = dt.sdt    
    form.ngay.data = dt.ngay
    return render_template('dattiec/updatetiec.html', form=form, sanh=sanh)

@app.route('/deletetiec/<int:id>', methods=['POST'])
def deletetiec(id):
    tc = Dattiec.query.get_or_404(id)
    if request.method =="POST":        
        db.session.delete(tc)
        db.session.commit()
        flash(f'{tc.tenchure} đã xóa','success')
        return redirect(url_for('dstieccuoi'))
    flash(f'Không thể xóa','success')
    return redirect(url_for('dstieccuoi'))

@app.route('/searchtieccuoi', methods=['GET','POST'])
def searchtieccuoi():
    if request.method=='POST':
        form=request.form
        search_value=form['search_string']
        search = "%{0}%".format(search_value)
        results = Dattiec.query.filter(or_(Dattiec.tenchure.like(search), Dattiec.tencodau.like(search))).all()
        return render_template('admin/dstieccuoi.html',tiec=results) 
    else:
        return redirect('dstieccuoi')