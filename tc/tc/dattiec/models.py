from wedding import db
from datetime import datetime


class Dattiec(db.Model):    
    tiec_id=db.Column(db.Integer,primary_key=True)
    tenchure=db.Column(db.String(50),nullable=False)
    tencodau=db.Column(db.String(50),nullable=False)
    sdt=db.Column(db.Integer,unique=True,nullable=False)        
    ngay=db.Column(db.DateTime, nullable=False, default=datetime.date)
    
    Sanh_id=db.Column(db.Integer, db.ForeignKey('sanh.sanh_id'),nullable=False)
    sanh=db.relationship('Sanh',backref=db.backref('sanhs', lazy=True))

    def __repr__(self):
        return '<Dattiec %r>' % self.tenchure

    
db.create_all()