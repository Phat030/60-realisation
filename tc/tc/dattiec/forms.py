from wtforms import Form, StringField, ValidationError, TextAreaField, validators, IntegerField
from .models import Dattiec
from wedding.sanh.models import Sanh
from flask_wtf import FlaskForm
from datetime import date
from wtforms.fields.html5 import DateField

class BookTiec(FlaskForm):
    tenchure=StringField('Tên chú rể',[validators.DataRequired()])
    tencodau=StringField('Tên cô dâu',[validators.DataRequired()])
    sdt=IntegerField('Số điện thoại', [validators.DataRequired()])    
    ngay=DateField('Ngày', [validators.DataRequired()] )    

    def validate_sdt(self, sdt):
        if Dattiec.query.filter_by(sdt=sdt.data).first():
            raise ValidationError('Số điện thoại đã tồn tại')

    def validate_ngay(self, ngay):
        if Dattiec.query.filter_by(ngay=ngay.data).first():
            raise ValidationError('Ngày đã được đặt')

    