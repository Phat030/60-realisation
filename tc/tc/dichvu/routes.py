from flask import flash,render_template, session, redirect, request, url_for
from wedding import app, db
from .models import Dichvu
from .forms import AddDichvu

import secrets
import os

@app.route('/dichvu', methods=['GET','POST'])
def dichvu():         
    form=AddDichvu(request.form)
    return render_template('dichvu/dichvu.html',form=form)

@app.route('/insertdichvu', methods = ['GET','POST'])
def insertdichvu():          
    form=AddDichvu()
    if form.validate_on_submit(): 
        ten = form.ten.data               
        soluong = form.soluong.data
        dongia = form.dongia.data        

        d = Dichvu(ten=ten, soluong=soluong, dongia=dongia)
        db.session.add(d)
        db.session.commit()
        
        flash(f'Thêm thành công','success') 
        return redirect(url_for('dsdichvu'))
    return render_template('dichvu/dichvu.html',form=form)

@app.route('/updatedichvu/<int:id>', methods = ['GET','POST'])
def updatedichvu(id):
    form = AddDichvu()
    dichvu = Dichvu.query.get_or_404(id)
    
    if form.validate_on_submit():
        dichvu.ten = form.ten.data         
        dichvu.soluong = form.soluong.data
        dichvu.dongia = form.dongia.data        
        flash('Menu đã được cập nhật','success')
        db.session.commit()
        return redirect(url_for('dsdichvu'))
    form.ten.data = dichvu.ten    
    form.soluong.data = dichvu.soluong
    form.dongia.data = dichvu.dongia    
    return render_template('dichvu/updatedichvu.html', form=form, dichvu=dichvu)

@app.route('/deletedichvu/<int:id>', methods=['POST'])
def deletedichvu(id):
    dichvu = Dichvu.query.get_or_404(id)
    if request.method =="POST":        
        db.session.delete(dichvu)
        db.session.commit()
        flash(f'{dichvu.ten} đã xóa','success')
        return redirect(url_for('dsdichvu'))
    flash(f'Không thể xóa dịch vụ','success')
    return redirect(url_for('dsdichvu'))

@app.route('/searchdichvu', methods=['GET','POST'])
def searchdichvu():
    if request.method=='POST':
        form=request.form
        search_value=form['search_string']
        search = "%{0}%".format(search_value)
        results = Dichvu.query.filter(Dichvu.ten.like(search)).all()
        return render_template('admin/dsdichvu.html',dichvu=results) 
    else:
        return redirect('dsdichvu')

@app.route('/searchdichvucus', methods=['GET','POST'])
def searchdichvucus():
    if request.method=='POST':
        form=request.form
        search_value=form['search_string']
        search = "%{0}%".format(search_value)
        results = Dichvu.query.filter(Dichvu.ten.like(search)).all()
        return render_template('customer/dsdichvu_customer.html',dichvu=results) 
    else:
        return redirect('dsdichvu_customer')
