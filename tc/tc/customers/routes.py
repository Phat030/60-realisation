from flask import flash,render_template, session, redirect, request, url_for
from werkzeug.security import generate_password_hash, check_password_hash
from wedding import app, db
from .forms import RegistrationForm, LoginForm
from .models import Customer
from wedding.sanh.models import Sanh
from wedding.menu.models import Menu
from wedding.dichvu.models import Dichvu

import secrets
import os


@app.route('/')
@app.route('/login_customer',methods=['GET','POST'])
def login_customer():
    form=LoginForm()
    if form.validate_on_submit():
        customer=Customer.query.filter_by(email=form.email.data).first()
        
        if customer:
            if check_password_hash(customer.password, form.password.data):                                   
                return redirect(url_for('homecustomer'))
            else:
                flash('Login Unsuccessful. Please check username and password', 'danger')
                return redirect(url_for('login_customer'))
    return render_template('customer/login_customer.html',form=form)

@app.route('/register_customer', methods=['GET','POST'])
def register_customer():
    form=RegistrationForm()
    if form.validate_on_submit():
        hashed_password=generate_password_hash(form.password.data, method='sha256')
        new_customer=Customer(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(new_customer)
        db.session.commit()
        return redirect(url_for('login_customer'))      
        
    return render_template('customer/register_customer.html',form=form)

@app.route('/homecustomer')
def homecustomer():     
    return render_template('homecustomer.html', title='Home')

@app.route('/dssanh_customer')
def dssanh_customer():     
    sanh=Sanh.query.all()
    return render_template('customer/dssanh_customer.html', title='Danh Sach Sanh Page', sanh=sanh)

@app.route('/lienhe')
def lienhe():
    return render_template('lienhe.html',title='Lien He')

@app.route('/dsmenu_customer')
def dsmenu_customer():
    menu=Menu.query.all()
    return render_template('customer/dsmenu_customer.html',title='Menu', menu=menu)

@app.route('/dsdichvu_customer')
def dsdichvu_customer():
    dichvu=Dichvu.query.all()
    return render_template('customer/dsdichvu_customer.html',title='Menu', dichvu=dichvu)