from wedding import db

class Customer(db.Model):
    id=db.Column(db.Integer, primary_key=True)
    username=db.Column(db.String(80),unique=True,nullable=False)
    email=db.Column(db.String(80),unique=True,nullable=False)
    password=db.Column(db.String(60),nullable=False) 

    def __repr__(self):
        return '<Customer %r>' % self.username

db.create_all()