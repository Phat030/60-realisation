from wtforms import BooleanField, StringField, PasswordField, validators , ValidationError, SubmitField
from flask_wtf import FlaskForm, Form
from wtforms.validators import DataRequired, Length, Email, EqualTo
from .models import Customer


class RegistrationForm(FlaskForm):    
    username = StringField('Username', [validators.Length(min=4, max=25)])
    email = StringField('Email Address', [validators.Length(min=6, max=35)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField('Repeat Password')
    submit=SubmitField('Sign up')

    def validate_username(self, username):
        if Customer.query.filter_by(username=username.data).first():
            raise ValidationError('username đã tồn tại')
    
    def validate_email(self, email):
        if Customer.query.filter_by(email=email.data).first():
            raise ValidationError('email đã tồn tại')


class LoginForm(FlaskForm):
    email=StringField('Email',validators=[DataRequired(),Email()])
    password=PasswordField('Password',validators=[DataRequired()])   
    remember=BooleanField('Remember me') 
    submit=SubmitField('Login')