from wedding import db
from flask_login import UserMixin
from .routes import login_manager

@login_manager.user_loader
def user_loader(user_id):
    return User.query.get(user_id)

class User(db.Model, UserMixin):
    id=db.Column(db.Integer, primary_key=True)
    username=db.Column(db.String(80),unique=True,nullable=False)
    email=db.Column(db.String(80),unique=True,nullable=False)
    password=db.Column(db.String(60),nullable=False) 

    def __repr__(self):
        return '<User %r>' % self.username

db.create_all()