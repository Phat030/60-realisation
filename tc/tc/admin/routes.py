from flask import flash,render_template, session, redirect, request, url_for
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_required, current_user, logout_user, login_user, logout_user
from wedding import app, db, login_manager
from .forms import RegistrationForm, LoginForm
from .models import User
from wedding.sanh.models import Sanh
from wedding.dattiec.models import Dattiec
from wedding.menu.models import Menu
from wedding.dichvu.models import Dichvu

import secrets
import os

@app.route('/')
@app.route('/login',methods=['GET','POST'])
def login():
    form=LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and check_password_hash(user.password, form.password.data):
            login_user(user)
            flash('You are login now!', 'success')
            next = request.args.get('next')
            return redirect(next or url_for('home'))
        flash('Incorrect email and password','danger')
        return redirect(url_for('login'))
            
    return render_template('admin/login.html', form=form)

@app.route('/register', methods=['GET','POST'])
def register():
    form=RegistrationForm()
    if form.validate_on_submit():
        hashed_password=generate_password_hash(form.password.data, method='sha256')
        new_user=User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(new_user)
        db.session.commit()
        return redirect(url_for('login'))      
        
    return render_template('admin/register.html',form=form)

@app.route('/logout')
def logout():
    logout_user()
    flash('You are logout now!', 'success')
    return redirect(url_for('login'))

@app.route('/home')
def home():     
    return render_template('home.html', title='Home')

@app.route('/dssanh')
def dssanh():     
    sanh=Sanh.query.all()
    return render_template('admin/dssanh.html', title='Danh Sach Sanh Page', sanh=sanh)

@app.route('/dstieccuoi')
def dstieccuoi():
    tiec=Dattiec.query.all()
    return render_template('admin/dstieccuoi.html', tiec=tiec)

@app.route('/dsmenu')
def dsmenu():
    menu=Menu.query.all()
    return render_template('admin/dsmenu.html', menu=menu)

@app.route('/dsdichvu')
def dsdichvu():
    dichvu=Dichvu.query.all()
    return render_template('admin/dsdichvu.html', dichvu=dichvu)