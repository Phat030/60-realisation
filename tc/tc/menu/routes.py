from flask import flash,render_template, session, redirect, request, url_for
from wedding import app, db
from .models import Menu
from .forms import AddMenu

import secrets
import os

@app.route('/menu', methods=['GET','POST'])
def menu():         
    form=AddMenu(request.form)
    return render_template('menu/menu.html',form=form)

@app.route('/insertmenu', methods = ['GET','POST'])
def insertmenu():          
    form=AddMenu()
    if form.validate_on_submit(): 
        ten = form.ten.data               
        soluong = form.soluong.data
        dongia = form.dongia.data        

        m = Menu(ten=ten, soluong=soluong, dongia=dongia)
        db.session.add(m)
        db.session.commit()
        
        flash(f'Thêm thành công','success') 
        return redirect(url_for('dsmenu'))
    return render_template('menu/menu.html',form=form)

@app.route('/updatemenu/<int:id>', methods = ['GET','POST'])
def updatemenu(id):
    form = AddMenu()
    menu = Menu.query.get_or_404(id)
    
    if form.validate_on_submit():
        menu.ten = form.ten.data         
        menu.soluong = form.soluong.data
        menu.dongia = form.dongia.data        
        flash('Menu đã được cập nhật','success')
        db.session.commit()
        return redirect(url_for('dsmenu'))
    form.ten.data = menu.ten    
    form.soluong.data = menu.soluong
    form.dongia.data = menu.dongia    
    return render_template('menu/updatemenu.html', form=form, menu=menu)

@app.route('/deletemenu/<int:id>', methods=['POST'])
def deletemenu(id):
    menu = Menu.query.get_or_404(id)
    if request.method =="POST":        
        db.session.delete(menu)
        db.session.commit()
        flash(f'{menu.ten} đã xóa','success')
        return redirect(url_for('dsmenu'))
    flash(f'Không thể xóa sảnh','success')
    return redirect(url_for('dsmenu'))

@app.route('/searchmenu', methods=['GET','POST'])
def searchmenu():
    if request.method=='POST':
        form=request.form
        search_value=form['search_string']
        search = "%{0}%".format(search_value)
        results = Menu.query.filter(Menu.ten.like(search)).all()
        return render_template('admin/dsmenu.html',menu=results) 
    else:
        return redirect('dsmenu')

@app.route('/searchmenucus', methods=['GET','POST'])
def searchmenucus():
    if request.method=='POST':
        form=request.form
        search_value=form['search_string']
        search = "%{0}%".format(search_value)
        results = Menu.query.filter(Menu.ten.like(search)).all()
        return render_template('customer/dsmenu_customer.html',menu=results) 
    else:
        return redirect('dsmenu_customer')

