from wtforms import Form, StringField, ValidationError, TextAreaField, validators, IntegerField
from .models import Menu
from flask_wtf import FlaskForm
class AddMenu(FlaskForm):
    ten=StringField('Tên',[validators.DataRequired()])    
    soluong=IntegerField('Số lượng', [validators.DataRequired()])
    dongia=IntegerField('Đơn giá', [validators.DataRequired()])    

    def validate_ten(self, ten):
        if Menu.query.filter_by(ten=ten.data).first():
            raise ValidationError('Đã tồn tại')