from wedding import db

class Menu(db.Model):    
    menu_id=db.Column(db.Integer,primary_key=True)
    ten=db.Column(db.String(50),unique=True,nullable=False)     
    soluong=db.Column(db.Integer,nullable=False)
    dongia=db.Column(db.Integer,nullable=False)       

    def __repr__(self):
        return '<Menu %r>' % self.ten

    
db.create_all()