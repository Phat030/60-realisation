from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager



app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test3.db'
app.config['SECRET_KEY']='Do not talk'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view='adminLogin'
login_manager.needs_refresh_message_category='danger'
login_manager.login_message = u"Please login first"


from wedding.admin import routes  
from wedding.sanh import routes
from wedding.customers import routes
from wedding.dattiec import routes
from wedding.menu import routes
from wedding.dichvu import routes