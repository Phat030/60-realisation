from flask import flash,render_template, session, redirect, request, url_for
from wedding import app, db
from .models import Sanh
from .forms import AddSanh

import secrets
import os

@app.route('/sanh', methods=['GET','POST'])
def sanh():         
    form=AddSanh(request.form)
    return render_template('sanh/sanh.html',form=form)

@app.route('/insertsanh', methods = ['GET','POST'])
def insertsanh():          
    form=AddSanh()
    if form.validate_on_submit(): 
        tensanh = form.tensanh.data
        loaisanh= form.loaisanh.data       
        soluongbantoida = form.soluongbantoida.data
        dongiaban = form.dongiaban.data
        ghichu = form.ghichu.data

        s = Sanh(tensanh=tensanh, loaisanh=loaisanh, soluongbantoida=soluongbantoida, dongiaban=dongiaban, ghichu=ghichu)
        db.session.add(s)
        db.session.commit()
        
        flash(f'Thêm thành công','success') 
        return redirect(url_for('dssanh'))
    return render_template('sanh/sanh.html',form=form)

@app.route('/updatesanh/<int:id>', methods = ['GET','POST'])
def updatesanh(id):
    form = AddSanh()
    sanh = Sanh.query.get_or_404(id)
    
    if form.validate_on_submit():
        sanh.tensanh = form.tensanh.data 
        sanh.loaisanh = form.loaisanh.data
        sanh.soluongbantoida = form.soluongbantoida.data
        sanh.dongiaban = form.dongiaban.data
        sanh.ghichu = form.ghichu.data
        flash('Sảnh đã được cập nhật','success')
        db.session.commit()
        return redirect(url_for('dssanh'))
    form.tensanh.data = sanh.tensanh
    form.loaisanh.data = sanh.loaisanh
    form.soluongbantoida.data = sanh.soluongbantoida
    form.dongiaban.data = sanh.dongiaban
    form.ghichu.data = sanh.ghichu
    return render_template('sanh/updatesanh.html', form=form, sanh=sanh)

@app.route('/deletesanh/<int:id>', methods=['POST'])
def deletesanh(id):
    sanh = Sanh.query.get_or_404(id)
    if request.method =="POST":        
        db.session.delete(sanh)
        db.session.commit()
        flash(f'{sanh.tensanh} đã xóa','success')
        return redirect(url_for('dssanh'))
    flash(f'Không thể xóa sảnh','success')
    return redirect(url_for('dssanh'))

@app.route('/search', methods=['GET','POST'])
def search():
    if request.method=='POST':
        form=request.form
        search_value=form['search_string']
        search = "%{0}%".format(search_value)
        results = Sanh.query.filter(Sanh.tensanh.like(search)).all()
        return render_template('admin/dssanh.html',sanh=results) 
    else:
        return redirect('dssanh')

@app.route('/searchcustomer', methods=['GET','POST'])
def searchcustomer():
    if request.method=='POST':
        form=request.form
        search_value=form['search_string']
        search = "%{0}%".format(search_value)
        results = Sanh.query.filter(Sanh.tensanh.like(search)).all()
        return render_template('customer/dssanh_customer.html',sanh=results) 
    else:
        return redirect('dssanh_customer')


