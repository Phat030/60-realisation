from wtforms import Form, StringField, ValidationError, TextAreaField, validators, IntegerField
from .models import Sanh
from flask_wtf import FlaskForm
class AddSanh(FlaskForm):
    tensanh=StringField('Tên sảnh',[validators.DataRequired()])
    loaisanh=StringField('Loại', [validators.DataRequired()])
    soluongbantoida=IntegerField('Số lượng bàn', [validators.DataRequired()])
    dongiaban=IntegerField('Đơn giá bàn', [validators.DataRequired()])
    ghichu=TextAreaField('Ghi chú', [validators.DataRequired()])

    def validate_tensanh(self, tensanh):
        if Sanh.query.filter_by(tensanh=tensanh.data).first():
            raise ValidationError('Tên sảnh đã tồn tại')