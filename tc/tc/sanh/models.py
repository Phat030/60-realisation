from wedding import db

class Sanh(db.Model):    
    sanh_id=db.Column(db.Integer,primary_key=True)
    tensanh=db.Column(db.String(50),unique=True,nullable=False)
    loaisanh=db.Column(db.String(10),nullable=False)    
    soluongbantoida=db.Column(db.Integer,nullable=False)
    dongiaban=db.Column(db.Integer,nullable=False)   
    ghichu=db.Column(db.String(100),nullable=False)

    def __repr__(self):
        return '<Sanh %r>' % self.tensanh

    
db.create_all()